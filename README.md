# arch-daemons

Collection of rc scripts for arch

### What is that?

arch leenucks can be used with CRUX init. it needs rc scripts. default are provided in git form, but it's hard to copy all those files from separate dirs, so I packed it into tarballs

### Installing

just execute those commands as root

    cd /tmp
    
    wget https://codeberg.org/glowiak/arch-daemons/raw/branch/master/rc.d.tar

    wget https://codeberg.org/glowiak/arch-daemons/raw/branch/master/conf.d.tar

    tar xvf rc.d.tar -C /etc/rc.d/

    tar xvf conf.d.tar -C /etc/conf.d/

    chmod 775 /etc/rc.d/*

### how?

I packed those scripts into tarballs with some shell commands